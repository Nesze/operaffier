# OperaFFier

## This script can be used to patch Opera on Ubuntu based platforms, to make it able to play H.264 videos (eg. on Twitter)

### Usage:
- Navigate in the menu by entering the option number, then pressing enter.

### Executing this pach requires
- root permissions
- the *apt-get* command line tool

### The script can
- install the *chromium-codecs-ffmpeg-extra* package
- create a backup of *libffmpeg.so* in your Opera installation folder under */usr/lib/x86_64-linux-gnu/opera*
- restore a previous backup
- copy *libffmpeg.so* either from */usr/lib/chromium-browser* or from */snap/chromium-ffmpeg/current/chromium-ffmpeg-`<latest_version>`/chromium-ffmpeg* to the Opera installation directory

### Known issues:
- In case the *chromium-codecs-ffmpeg-extra* installation finishes, but the patch fails, try removing the package (with *apt-get remove chromium-codecs-ffmpeg-extra*), and then run the script again to reinstall it.

I tried to be as careful as possible while writing the script, but I don't take responsibility for failing browsers, late nights or graying hair. As far as I'm concerned, nothing should go wrong if you have read the "*The script can*" section above. Happy patching!

### Changelog:

v1.1.0
- The script now detects if chromium is installed as a snap

v1.0.2
- Fixed use of undefined variable when copying *libffmeg.so* (Issue#1)

v1.0.1
- Bug fixes regarding the menu system

v1.0.0
- Initial version
